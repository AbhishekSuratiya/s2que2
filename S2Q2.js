/*
  Q.	Write a Program that should contains
  1. Class Person with fields (name, age, salary, sex)
  2. a static sort function in the class that should take array of Persons and name of the field and order of sorting and should return a new sorted array based on above inputs. Should not change intial array.

  for example Person.sort(arr, 'name', 'asc') -> sort array of persons based on name in ascending order. 'desc' for descending

  3. You have to write Quick sort for this sorting algorithms.
*/

//Solution One
// class Person {
//   constructor(name, age, salary, sex) {
//     this.name = name
//     this.age = age
//     this.salary = salary
//     this.sex = sex
//   }

//   static customSort(array, name, order) {

//     var arrayCopy = [...array]

//     if (name === 'age' || name === 'salary') {
//       function compare(a, b) {
//         if (order === 'asc' && name === 'age')
//           return a.age - b.age
//         if (order === 'desc' && name === 'age')
//           return b.age - a.age
//         if (order === 'asc' && name === 'salary')
//           return a.salary - b.salary
//         if (order === 'desc' && name === 'salary')
//           return b.salary - a.salary
//       }
//       console.log('Sorted Array', arrayCopy.sort(compare));
//     }

//     if (name === 'name' || name === 'sex') {
//       function compare(a, b) {
//         if (name === 'name')
//           if (a.name < b.name) return -1
//         if (name === 'name')
//           if (a.name > b.name) return 1
//         if (name === 'sex')
//           if (a.sex < b.sex) return -1
//         if (name === 'sex')
//           if (a.sex > b.sex) return 1
//         return 0
//       }

//       if (order === 'desc')
//         console.log('Sorted Array', arrayCopy.sort(compare).reverse());
//       else
//         console.log('Sorted Array', arrayCopy.sort(compare));
//     }
//   }

// }

// var person1 = new Person('abhishek', 24, 1200, 'M')
// var person2 = new Person('deepak', 26, 22000, 'M')
// var person3 = new Person('bablu', 34, 2000, 'M')
// var person4 = new Person('rekha', 20, 600, 'F')
// var person5 = new Person('savita', 44, 8000, 'F')

// var personArray = [person1, person2, person3, person4, person5]

// Person.customSort(personArray, 'name', 'desc');

// console.log('Orignal Array', personArray);


// Solution 2

class Person {
  constructor(name, age, salary, sex) {
    this.name = name
    this.age = age
    this.salary = salary
    this.sex = sex
  }
}

var person1 = new Person('abhishek', 24, 1200, 'M')
var person2 = new Person('deepak', 26, 22000, 'M')
var person3 = new Person('bablu', 34, 2000, 'M')
var person4 = new Person('rekha', 20, 600, 'F')
var person5 = new Person('savita', 44, 8000, 'F')

var personArray = [person1, person2, person3, person4, person5]

//****************************************************************************** */
function customSort(arrayCopy, property, order) {
  var array = [...arrayCopy];

  if (property === 'age') {
    var propArray = array.map((val) => {
      return val.age;
    })
  }
  if (property === 'salary') {
    var propArray = array.map((val) => {
      return val.salary;
    })
  }

  var items = propArray;
  //Quick Sort algo start
  function swap(items, leftIndex, rightIndex) {
    var temp = items[leftIndex];
    items[leftIndex] = items[rightIndex];
    items[rightIndex] = temp;
  }
  function partition(items, left, right) {
    var pivot = items[Math.floor((right + left) / 2)],
      i = left,
      j = right;
    while (i <= j) {
      while (items[i] < pivot) {
        i++;
      }
      while (items[j] > pivot) {
        j--;
      }
      if (i <= j) {
        swap(items, i, j);
        i++;
        j--;
      }
    }
    return i;
  }

  function quickSort(items, left, right) {
    var index;
    if (items.length > 1) {
      index = partition(items, left, right);
      if (left < index - 1) {
        quickSort(items, left, index - 1);
      }
      if (index < right) {
        quickSort(items, index, right);
      }
    }
    return items;
  }
  //Quicksort algo finish

  var sortedPropArray = quickSort(items, 0, items.length - 1);
  var sortedObjArray = [];
  for (let i = 0; i < sortedPropArray.length; i++) {
    array.forEach((val) => {
      if (property === 'age' && val.age === sortedPropArray[i]) {
        sortedObjArray.push(val)
      }
      if (property === 'salary' && val.salary === sortedPropArray[i]) {
        sortedObjArray.push(val)
      }
    })
  }

  order === 'asc' ? console.log("Sorted Array", sortedObjArray) : null
  order === 'desc' ? console.log("Sorted Array", sortedObjArray.reverse()) : null

}
//****************************************************************************** */


console.log('Orignal Array', personArray);
customSort(personArray, 'salary', 'desc')
